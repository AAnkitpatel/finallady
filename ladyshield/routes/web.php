<?php

Route::get('/clear-cache', function() {
$exitCode = Artisan::call('cache:clear');
return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
$exitCode = Artisan::call('optimize');
return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
$exitCode = Artisan::call('route:cache');
return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
$exitCode = Artisan::call('route:clear');
return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
$exitCode = Artisan::call('view:clear');
return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
$exitCode = Artisan::call('config:cache');
return '<h1>Clear Config cleared</h1>';
});

Route::get('download/{filename}', function($filename)
{
    // Check if file exists in app/storage/file folder
    $file_path = public_path() .'/uploads/users/'. $filename;
    if (file_exists($file_path))
    {
        // Send Download
        return Response::download($file_path, $filename, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
})
->where('filename', '[A-Za-z0-9\-\_\.]+');


Route::get('terms-condition', 'HomeController@term');
Route::get('privacy-policy', 'HomeController@privacy');
Route::get('privacy', 'HomeController@privacy');
Route::get('support', 'HomeController@support');
Route::redirect('/', '/login');

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy'); 
    Route::resource('users', 'UsersController'); 
    Route::get('/admin-edit/{id}', 'UsersController@adminedit')->name('adminedit');
    Route::post('/admin-edit', 'UsersController@adminedit')->name('adminpost');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');
    //// files ////

    Route::delete('files/destroy', 'FilesController@massDestroy')->name('files.massDestroy');

    Route::resource('files', 'FilesController');

    Route::get('get/{filename}', 'FilesController@getFile')->name('getfile');

    //// locations ////

    Route::delete('locations/destroy', 'LocationsController@massDestroy')->name('locations.massDestroy');

    Route::resource('locations', 'LocationsController');

    Route::delete('page/destroy', 'PageController@massDestroy')->name('page.massDestroy'); 
    Route::resource('pages', 'PageController'); 

});
