@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }} 
  
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.user.fields.name') }}
                    </th>
                    <td>
                        {{ $user->first_name." ".$user->last_name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.email') }}
                    </th>
                    <td>
                        {{ $user->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.number') }}
                    </th>
                    <td>
                        {{ $user->phone_number }}
                    </td>
                </tr>

             <?php
               $user_role =auth()->user()->roles()->first(); 

                if( $user->id==1) { 
              ?>
                <tr>
                    <th>
                         Update
                    </th>
                    <td>
                         <a href="{{url("admin/admin-edit/".auth()->user()->id)}}"> <button class="btn btn-danger">update </button></a>
                    </td>
                </tr>

            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

@endsection