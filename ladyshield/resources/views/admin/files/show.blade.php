@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.product.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.file.fields.media') }}
                    </th>
                    <td>
                     <?php $type =($file->media==1)?'picture':'video';?>
                        {{ $type }}
                    </td>
                </tr> 
                <tr>
                    <th>
                        File Name   <img src="{!! url("/api/public".$file->path) !!}" alt="{{$file->filename}}">
                    </th>
                    <td>
                        {{ $file->filename }}
                    </td>
                    
                </tr>


                <tr>
                    <th>
                        {{ trans('global.file.fields.path') }}
                    </th>
                    <td> 
                        @if($type==1)
                          <img src="{!! $file->path !!}" alt="{{$file->filename}}">
                         @elseif($type == 3)
                          <a class="btn btn-xs btn-primary" href="{{$file->path}}" target="_blank">Click here</a> 
                         @endif
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

@endsection