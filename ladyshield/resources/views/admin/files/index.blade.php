@extends('layouts.admin')
@section('content')
@can('product_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
             <a class="btn btn-success" href="{{ route('admin.users.show',$user->id) }}">
                 {{$user->first_name}} {{$user->last_name}}
            </a> 
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('global.file.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            Media Type
                        </th>
                        <th>
                           File Name
                        </th>

                        <th>
                            {{ trans('global.file.fields.path') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($files as $key => $file)
                        <tr data-entry-id="{{ $file->id }}">
                            <td>

                            </td>
                            <td>
                                <?php $type =($file->media==1)?'picture':'video';?>
                                  {{ $type }}
                            </td>
                            <td>
                                {{ $file->filename ?? '' }}
                            </td>
                            <td> 
                               <a class="btn btn-xs btn-primary" href="{{$file->path}}" target="_blank">View file</a>
                            </td>
                            <td> 

                                @if($file->filename)
                                    <a class="btn btn-xs btn-primary" href=" {{ url('download/'.$file->filename) }}">
                                        <i class="fa fa-download" aria-hidden="true"></i>Download
                                    </a> 
                                @endif

                                  <!--   <a class="btn btn-xs btn-primary" href="{{ route('admin.files.show', $file->id) }}">
                                        {{ trans('global.view') }}
                                    </a>  -->
                                
                                    <form action="{{ route('admin.files.destroy', $file->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.files.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('product_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection