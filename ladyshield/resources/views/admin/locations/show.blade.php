@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.location.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.location.fields.url') }}
                    </th>
                    <td>
                         <a class="btn btn-red" href="{{ $location->url }}" target="_blank">
                              Show location
                        </a >  
                    </td>
                </tr>
                 
            </tbody>
        </table>
         <div id="embedMap">
        <!--Google map will be embedded here-->
    </div>
    </div>
</div>
<script>
    
        // Get location data
        var latlong =  '28.664419' + "," + '77.376671';
        
        // Set Google map source url
        var mapLink = "https://maps.googleapis.com/maps/api/staticmap?center="+latlong+"&zoom=16&size=400x300&output=embed";
        
        // Create and insert Google map
        document.getElementById("embedMap").innerHTML = "<img alt='Map Holder' src='"+ mapLink +"'>";
    
</script>

@endsection