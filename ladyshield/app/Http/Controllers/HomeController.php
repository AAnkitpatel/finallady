<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function term(){
        $term = Page::where('id',1)->first();
        return view('term',compact('term'));
    }

    public function privacy(){
        $privacy = Page::where('id',2)->first();
        return view('privacy',compact('privacy'));
    }


    public function support(){
        $support = Page::where('id',5)->first();
        return view('support',compact('support'));
    }
}
