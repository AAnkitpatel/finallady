<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyLocationRequest;
use App\Http\Requests\StoreLocationRequest;
use App\Http\Requests\UpdateLocationRequest;
use App\Location;
use App\User;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function index(Request $request)
    {
        //abort_unless(\Gate::allows('Location_access'), 403);
          $user_id = $request->input('user_id');
            $user  = User::find($user_id);

        $locations = Location::where('user_id',$user_id)->get();

        return view('admin.locations.index', compact('locations','user'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('Location_create'), 403);

        return view('admin.locations.create');
    }

    public function store(StoreLocationRequest $request)
    {
        //abort_unless(\Gate::allows('Location_create'), 403);

        $Location = Location::create($request->all());

        return redirect()->route('admin.locations.index');
    }

    public function edit(Location $Location)
    {
       // abort_unless(\Gate::allows('Location_edit'), 403);

        return view('admin.locations.edit', compact('locations'));
    }

    public function update(UpdateLocationRequest $request, Location $Location)
    {
        //abort_unless(\Gate::allows('Location_edit'), 403);

        $Location->update($request->all());

        return redirect()->route('admin.locations.index');
    }

    public function show(Location $location)
    {
        //abort_unless(\Gate::allows('Location_show'), 403);
       

        return view('admin.locations.show', compact('location'));
    }

    public function destroy(Location $Location)
    {
        //abort_unless(\Gate::allows('Location_delete'), 403);

        $Location->delete();

        return back();
    }

    public function massDestroy(MassDestroyLocationRequest $request)
    {
        Location::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
