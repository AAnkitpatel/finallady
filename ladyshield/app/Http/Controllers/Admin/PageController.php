<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StorePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Role;
use App\User;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function __construct()
    {

      //die('hi..');
    }
    public function index()
    {
        //abort_unless(\Gate::allows('user_access'), 403);

        //$users = User::all();
         $pages = Page::where('is_deleted',0)->get();

        return view('admin.pages.index', compact('pages'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('user_create'), 403);

        $roles = Role::all()->pluck('title', 'id');

        return view('admin.pages.create', compact('roles'));
    }

    public function store(StorePageRequest $request)
    {

        $user = Page::create($request->all());

        return redirect()->route('admin.pages.index');
    }

    public function edit(Page $page)
     {
        //abort_unless(\Gate::allows('user_edit'), 403);

        //$roles = Role::all()->pluck('title', 'id'); 

        //$user->load('roles');

        return view('admin.pages.edit', compact('page'));
    }

    public function adminedit(request $request,$id)
     {
        abort_unless(\Gate::allows('user_edit'), 403);

        $user = User::find($id);
        $roles = Role::all()->pluck('title', 'id');  
        $user->load('roles');

        return view('admin.pages.admin-edit', compact('roles', 'user'));
    }

    public function update(UpdatePageRequest $request, Page $page)
    {
         

        //abort_unless(\Gate::allows('user_edit'), 403);

        $page->update($request->all());

        //$user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.pages.index');
    }

    public function show(User $user)
    {
        abort_unless(\Gate::allows('user_show'), 403);

        $user->load('roles');

        return view('admin.pages.show', compact('user'));
    }

    public function destroy(Page $page)
    {
        //abort_unless(\Gate::allows('user_delete'), 403);

        $page->is_deleted = 1;
        $page->save();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
