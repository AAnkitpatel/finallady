<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFileRequest;
use App\Http\Requests\StoreFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\File;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\Response as FacadeResponse;

class FilesController extends Controller
{
    public function index(Request $request)
    {
        //abort_unless(\Gate::allows('File_access'), 403);

        $files = File::all();
        $user_id = $request->input('user_id');
        $user  = User::find($user_id);

        return view('admin.files.index', compact('files','user'));
    }

    public function create()
    {
        //abort_unless(\Gate::allows('File_create'), 403);

        return view('admin.files.create');
    }

    public function store(StoreFileRequest $request)
    {
        //abort_unless(\Gate::allows('File_create'), 403);

        $File = File::create($request->all());

        return redirect()->route('admin.files.index');
    }

    public function edit(File $File)
    {
        //abort_unless(\Gate::allows('File_edit'), 403);

        return view('admin.files.edit', compact('files'));
    }

    public function update(UpdateFileRequest $request, File $File)
    {
        //abort_unless(\Gate::allows('File_edit'), 403);

        $File->update($request->all());

        return redirect()->route('admin.files.index');
    }

    public function show(File $file)
    {
       // abort_unless(\Gate::allows('File_show'), 403);

        return view('admin.files.show', compact('file'));
    }

    public function destroy(File $File)
    {
        //abort_unless(\Gate::allows('File_delete'), 403);

        $File->delete();

        return back();
    }

    public function massDestroy(MassDestroyFileRequest $request)
    {
        File::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }

    function getFile($filename){

        $file= url('uploads/users/'.$filename); 
        return response()->download($file); 
    }
}
