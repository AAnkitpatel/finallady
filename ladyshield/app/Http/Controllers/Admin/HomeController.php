<?php

namespace App\Http\Controllers\Admin;
use App\User;

class HomeController
{
    public function index()
    {
    	 
       $users = User::where('id', '!=', auth()->user()->id)->get();

        return view('admin.users.index', compact('users'));
    }
}
